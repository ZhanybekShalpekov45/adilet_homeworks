from db import sql_queries
import sqlite3


class Database:
    def __init__(self):
        self.connection = sqlite3.connect("db.sqlite")
        self.cursor = self.connection.cursor()

    def sql_create_table(self):
        if self.connection:
            print("Подключение к базе данных успешно!")
        self.connection.execute("CREATE TABLE IF NOT EXISTS geek_students (id INT, username TEXT, first_name TEXT, last_name TEXT)")
        self.connection.commit()

    def sql_insert_into_table(self, id, username, first_name, last_name):
        self.cursor.execute("INSERT INTO geek_students (id, username, first_name, last_name) VALUES (?, ?, ?, ?)",
                            (id, username, first_name, last_name))
        self.connection.commit()

    def sql_insert_answers_quiz(self, id_user, quiz, quiz_option):
        self.cursor.execute("INSERT INTO answers_quiz (id_user, quiz, quiz_option) VALUES (?, ?, ?)",
                            (id_user, quiz, quiz_option))
        self.connection.commit()


