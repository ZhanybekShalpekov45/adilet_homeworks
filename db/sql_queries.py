CREATE_USER_TABLE = '''
CREATE TABLE IF NOT EXISTS geek_students(
id INTEGER PRIMARY KEY,
username CHAR(32),
first_name CHAR(64),
last_name CHAR(64),
)'''


INSERT_INTO_TABLE = '''
INSERT OR IGNORE INTO geek_students VALUES (?,?,?)
'''

CREATE_ANSWER_QUIZ = """
        CREATE TABLE IF NOT EXISTS answers_quiz
        (id INTEGER PRIMARY KEY AUTOINCREMENT, id_user CHAR(10),
        quiz CHAR(10), quiz_option INTEGER,
        FOREIGN KEY (id_user) REFERENCES id)
"""

insert_answers_quiz = """
INSERT INTO answers_quiz(id_user, quiz, quiz_option) VALUES (?, ?, ?)
"""