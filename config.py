from dotenv import load_dotenv
from aiogram import Bot,Dispatcher
import os
import logging


logging.basicConfig(level=logging.INFO)
load_dotenv()
bot = Bot(os.getenv('TOKEN'))
dp = Dispatcher(bot=bot)

