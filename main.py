from config import dp
from aiogram.utils import executor
from handlers import start, chat_actions
from db import sql_commands



start.register_start_handlers(dp)


async def on_start_up(_):
    db = sql_commands.Database()
    db.sql_create_table()
    print("Бот готов!")




if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_start_up)
